function [Z] = decode( filename )
%DECODE Summary of this function goes here
%   Detailed explanation goes here


input = [filename( 1:5 )  'cmp.mat'];

output = [filename( 1:5 )  'dec.mat'];

load( input );

vlc = vlc(:,1)';

Z = optimal_decode( vlc, dict, stepbase, scheme );

draw( Z );

save( output, 'Z' );

end

