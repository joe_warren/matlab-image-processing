function [size, qual] = dwt_compression_compare( X, rise )
%DWT_COMPRESSION_COMPAIR Summary of this function goes here
%   Detailed explanation goes here


global image;
image = X;
global steprise;
steprise = rise;
stepbase = fzero( @dwt_size, 6000 )

[vlc, dict] = dwt_encode(X, stepbase, rise);

size = ( numel(vlc)/8 + 5 * (numel(dict)/2) ) / 1024;

Z = dwt_decode( vlc, dict, stepbase, rise );

[qual, map] = ssim_index( X, Z+128 );   

draw( beside( Z, 128*map./max(map(:)) ) );

end

