function [ vlc, dict, stepbase, size ] = lbt_optimal_encode( X )
%LB_OPTIMAL_ENCODE Summary of this function goes here
%   Detailed explanation goes here

rise = 1.0;

global image;
image = X;
global steprise;
steprise = rise;

stepbase = fzero( @lbt_size, 50 );

[vlc, dict] = lbt_encode(X, stepbase, rise);

size = ( numel(vlc)/8 + 5 * (numel(dict)/2) ) / 1024;

end

