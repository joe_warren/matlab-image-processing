function [P1 P2 P3 P4 ] = quantized_pyramids( X )
%QUANTIZED_PYRAMIDS Summary of this function goes here
%   Detailed explanation goes here

% create the pyramids for X0 through to X4  

h = [1 4 6 4 1]/16;

step = 17;

X1 = rowdec( rowdec( X, h )', h )';
Y0 = X - rowint( rowint( X1, 2 .* h )', 2.*h )';

X2 = rowdec( rowdec( X1, h )', h )';
Y1 = X1 - rowint( rowint( X2, 2 .* h )', 2.*h )';

X3 = rowdec( rowdec( X2, h )', h )';
Y2 = X2 - rowint( rowint( X3, 2 .* h )', 2.*h )';

X4 = rowdec( rowdec( X3, h )', h )';
Y3 = X3 - rowint( rowint( X4, 2 .* h )', 2.*h )';

% Quantize

X1 = quantise( X1, step/1.093 );
X2 = quantise( X2, step/1.976 );
X3 = quantise( X3, step/3.863 );
X4 = quantise( X4, step/7.684 );

Y0 = quantise( Y0, step );
Y1 = quantise( Y1, step/1.093 );
Y2 = quantise( Y2, step/1.976 );
Y3 = quantise( Y3, step/3.863 );

% create images from the Pyramids

% 4th Pyramid

Z3 = Y3 + rowint( rowint( X4, h*2 )', h*2 )';

Z2 = Y2 + rowint( rowint( Z3, h*2 )', h*2 )';

Z1 = Y1 + rowint( rowint( Z2, h*2 )', h*2 )';

P4 = Y0 + rowint( rowint( Z1, h*2 )', h*2 )';

% 3rd Pyramid


Z2 = Y2 + rowint( rowint( X3, h*2 )', h*2 )';

Z1 = Y1 + rowint( rowint( Z2, h*2 )', h*2 )';

P3 = Y0 + rowint( rowint( Z1, h*2 )', h*2 )';

% 2nd Pyramid

Z1 = Y1 + rowint( rowint( X2, h*2 )', h*2 )';

P2 = Y0 + rowint( rowint( Z1, h*2 )', h*2 )';

% 1st Pyramid

P1 = Y0 + rowint( rowint( X1, h*2 )', h*2 )';

% display the Pyramids along with X

draw( beside( X, beside( P1, beside( P2, beside( P3, P4 ) ) ) ) );

% calculate bits per layer

x1_bits_quantized = bpp( X1 ) * numel( X1 );
y0_bits_quantized = bpp( Y0 ) * numel( Y0 );
x2_bits_quantized = bpp( X2 ) * numel( X2 );
y1_bits_quantized = bpp( Y1 ) * numel( Y1 );
x3_bits_quantized = bpp( X3 ) * numel( X3 );
y2_bits_quantized = bpp( Y2 ) * numel( Y2 );
x4_bits_quantized = bpp( X4 ) * numel( X4 );
y3_bits_quantized = bpp( Y3 ) * numel( Y3 );

Bits_in_Pyramid_1 = x1_bits_quantized + y0_bits_quantized 
Bits_in_Pyramid_2 = x2_bits_quantized + y0_bits_quantized + y1_bits_quantized 
Bits_in_Pyramid_3 = x3_bits_quantized + y0_bits_quantized + y1_bits_quantized + y2_bits_quantized
Bits_in_Pyramid_4 = x3_bits_quantized + y0_bits_quantized + y1_bits_quantized + y2_bits_quantized + y3_bits_quantized


% Display Errors

Diff_1 = X-P1;
RMS_Error_1 = sqrt(sum( Diff_1(:).^2 )/numel(Diff_1))

Diff_2 = X-P2;
RMS_Error_2 = sqrt(sum( Diff_2(:).^2 )/numel(Diff_2))

Diff_3 = X-P3;
RMS_Error_3 = sqrt(sum( Diff_3(:).^2 )/numel(Diff_3))

Diff_4 = X-P4;
RMS_Error_4 = sqrt(sum( Diff_4(:).^2 )/numel(Diff_4))
end

