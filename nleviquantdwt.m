function [ X ] = nleviquantdwt( Y, n, q, rise )
%NLEVIDWT Summary of this function goes here
%   Detailed explanation goes here


m = 256/(2^(n-1)); 

X = Y;

for i = 1:n
    t = 1:m/2;
    
    if( i == 1 )
        X(t, t) = quant2( X(t, t), q( 1, n+1 ), q( 1, n+1 )*rise ); 
    end
    
    X(m/2 + t, t) = quant2( X(m/2 + t, t), q( 1, n-i+1 ), q( 1, n-i+1 )*rise );
    X(t, m/2 + t) = quant2( X(t, m/2 + t), q( 2, n-i+1 ), q( 2, n-i+1 )*rise );
    X(m/2 + t, m/2 + t) = quant2( X(m/2 + t, m/2 + t), q( 3, n-i+1 ), q( 3, n-i+1 )*rise );
    
    t = 1:m;
    X(t, t) = idwt(X(t,t));
    m = m*2;
end

end

