function [Yr]=lbt_rearrange(Y)
scan=[1 diagscan(8)];
Yr=[];
for i=0:31
    for j=0:31
        Yi=Y((8*i+[1:8]),(8*j+[1:8]));
        Yr=[Yr;Yi(scan)];
    end

end
Yr=reshape(Yr,[],1);
end