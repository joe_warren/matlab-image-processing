function [Y]=lbt_irearrange(Yr)
scan=[1 diagscan(8)];
iscan(scan)=1:64;
Y=zeros(256);
Yr=reshape(Yr,1024,64);
for i=0:31
    for j=0:31
        Yi=Yr(j+32*i+1,:);
        Y((8*i+[1:8]),(8*j+[1:8]))=reshape(Yi(iscan),8,8);
    end

end
end