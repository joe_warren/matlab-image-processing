function [Y0 Y1 Y2 Y3 X4] = drawpy4enc(X);

[Y0 Y1 Y2 Y3 X4] = py4enc(X);

[Z3 Z2 Z1 Z0] = py4dec(Y0, Y1, Y2, Y3, X4);

draw( beside( Z0, beside( Z1, beside( Z2, Z3 ) ) ) );