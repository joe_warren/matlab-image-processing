function Yq = lbt_quantise( X, step, rise )
%DCT_QUANTISE Summary of this function goes here
%   Detailed explanation goes here

N=8;

s = 1.3;

C = dct_ii(N);

[Pf Pr] = pot_ii(N, s);

I = 256;
t= [(1+N/2):(I-N/2)];

Xp = X-128;
Xp(t,:) = colxfm( Xp(t,:), Pf);
Xp(:,t) = colxfm( Xp(:,t)', Pf)';

Y = colxfm( colxfm(Xp, C)', C )';

Yq = quant1( Y, step, step * rise );

end

