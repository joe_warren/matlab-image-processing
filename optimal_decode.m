function [ Z ] = optimal_decode( vlc, dict, stepbase, scheme)
%OPTIMAL_DECODE Summary of this function goes here
%   Detailed explanation goes here

if scheme == 1
    disp 'decoding wih dwt'
    Z = dwt_decode( vlc, dict,stepbase ,1.0 );
else
    disp 'decoding with lbt'
    Z = lbt_decode( vlc, dict,stepbase ,1.0 );
end
Z = Z + 128;

end

