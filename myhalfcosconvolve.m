function Y = myhalfcosconvolve( X )
%MYHALFCOSCONVOLVE Summary of this function goes here
%   Detailed explanation goes here

Y = [];
h = halfcos(15);
for i = 1:256
    y = conv( X( i, : ), h );
    Y = [Y; y];
end

draw( Y(:, [1:256] + 7 ) );
    