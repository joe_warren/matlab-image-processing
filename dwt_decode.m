function [ Z ] = dwt_decode( vlc, dict,basestep,rise )
%DWT_DECODE Summary of this function goes here
%   Detailed explanation goes here

n = 7;

HDEC = huffmandeco( vlc, dict );

RL = reshape( HDEC , numel(HDEC)/2 ,2  );

RLY = runlengthdec( RL );

Y = reshape( RLY, 256, 256 );

q = 1./sqrt( output_energies_from_dwt( n ) ) * basestep;

Z = nleviquantdwt( Y, n, q, rise );


end


