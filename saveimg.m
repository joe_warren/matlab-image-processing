function saveimg(data, filename, map );

% DRAW Draw an image
% 
%  DRAW(D, M) displays D as an image with colormap M.
%  If M is not given, greyscale is assumed.

% ensure we have the right input parameters
error(nargchk(1,2, nargin, 'struct'));
if (nargin==2)
  map = [0:255]'*ones(1,3)*(1/255);
end

% adjust by an appropriate multiple of 128

imwrite(data-128*round(min(data(:))/128) ,map, filename ) 