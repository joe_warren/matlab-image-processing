function enco = huffmanEnco(sig,dict) 
%     
%    Encodes the input signal, sig, based on the code dictionary, dict.  
%    The code dictionary is generated using the huffmanDict function. 
%-------------------------------------------------------------------------- 
 
% Make sure that the input symbols are in a cell array format 
if (~iscell(sig) ) 
	[m,n] = size(sig); 
	sig = mat2cell(sig, ones(1,m), ones(1,n) ); 
end 
 
% Preallocate memory for enco 
maxSize = 0; 
dictLength = size(dict,1); 
for i = 1 : dictLength 
    tempSize = size(dict{i,2},2); 
    if (tempSize > maxSize) 
        maxSize = tempSize; 
    end 
end 
enco = zeros(1, length(sig)*maxSize); 
 
% Search the dictionary sequentially to find the code for the given signal 
idxCode = 1; 
for i = 1 : length(sig) 
    tempcode = []; 
    for j = 1 : dictLength 
        if( sig{i} == dict{j,1} ) 
            tempcode = dict{j,2}; 
            break; 
        end 
    end  
    lenCode = length(tempcode); 
    enco(idxCode : idxCode+lenCode-1) = tempcode; 
    idxCode = idxCode + lenCode; 
end 
 
% Strip off the unused vector elements 
enco = enco(1:idxCode-1);

