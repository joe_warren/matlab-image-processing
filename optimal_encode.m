function [ vlc, dict, stepbase, scheme ] = optimal_encode( X )
%OPTIMAL_ENCODE Summary of this function goes here
%   Detailed explanation goes here

[vlcdwt dictdwt stepdwt sizedwt] = dwt_optimal_encode( X );


Zdwt = dwt_decode( vlcdwt, dictdwt,stepdwt ,1.0 );


[vlclbt dictlbt steplbt sizelbt] = lbt_optimal_encode( X );


Zlbt = lbt_decode( vlclbt, dictlbt,steplbt ,1.0 );


dwtq = ssim_index( X, Zdwt+128 )
lbtq = ssim_index( X, Zlbt+128 )

if dwtq > lbtq
    disp 'encoding using the discrete wavelet transform' 
    vlc = vlcdwt;
    dict = dictdwt;
    stepbase = stepdwt;
    size = sizedwt
    scheme = 1;
else
    disp 'encoding using the lapped bi-orthogonal transform' 
    vlc = vlclbt;
    dict = dictlbt;
    stepbase = steplbt;
    size = sizelbt
    scheme = 2;
end

end

