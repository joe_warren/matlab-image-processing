
lighthouse
lbt 0.5

stepbase =

   77.2854


s =

    4.9000


q =

    0.8420

dwt 0.5

stepbase =

   7.6986e+03


s =

    4.8973


q =

    0.8378

lighthouse
lbt 1.0

stepbase =

   42.5921


s =

    4.9028


q =

    0.8497

dwt 1.0

stepbase =

   4.2247e+03


s =

    4.8988


q =

    0.8370

lighthouse
lbt 1.5

stepbase =

   29.7422


s =

    4.8976


q =

    0.8492

dwt 1.5

stepbase =

   3.0414e+03


s =

    4.8989


q =

    0.8271

bridge
lbt 0.5

stepbase =

   89.9408


s =

    4.9001


q =

    0.7323

dwt 0.5

stepbase =

   8.5180e+03


s =

    4.9001


q =

    0.7063

bridge
lbt 1.0

stepbase =

   47.6414


s =

    4.8999


q =

    0.7298

dwt 1.0

stepbase =

   4.5366e+03


s =

    4.9000


q =

    0.6979

bridge
lbt 1.5

stepbase =

   33.3353


s =

    4.9003


q =

    0.7179

dwt 1.5

stepbase =

   3.1543e+03


s =

    4.9000


q =

    0.6869

flamingo
lbt 0.5

stepbase =

  107.0512


s =

    4.8999


q =

    0.7529

dwt 0.5

stepbase =

   9.4747e+03


s =

    4.9001


q =

    0.7644

flamingo
lbt 1.0

stepbase =

   59.1484


s =

    4.8997


q =

    0.7678

dwt 1.0

stepbase =

   5.2108e+03


s =

    4.9003


q =

    0.7761

flamingo
lbt 1.5

stepbase =

   41.3983


s =

    4.8997


q =

    0.7780

dwt 1.5

stepbase =

   3.6990e+03


s =

    4.8989


q =

    0.7647
