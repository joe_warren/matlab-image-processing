\documentclass[a4paper,12pt]{article}

%\usepackage{a4wide}
\usepackage[cm]{fullpage}
\usepackage{amsmath}

\usepackage{array}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{listings}
\usepackage{fixltx2e}
\usepackage[justification=centering]{caption}
\usepackage{subcaption}
\usepackage{emp}
\usepackage{multirow}
\usepackage{color}
\usepackage{xcolor}
\usepackage[miktex]{gnuplottex} 
\usepackage{epstopdf}

\usepackage{booktabs}

\usepackage[runin]{abstract}


\usepackage{tikz}

\usetikzlibrary{matrix, calc, arrows}

\usepackage{gensymb}

\renewcommand{\abstractname}{Summary: } 
\renewcommand{\absnamepos}{empty}

\begin{document}

\pagenumbering{roman}

\title{ SF2 :: Image Processing\\Final Report }
\author{Joseph Warren ( jw718 ) \\ Churchill College}
\date{22/5/14}

\maketitle


\begin{abstract}

This report covers the design of an image compression scheme, required to reduce a $256\times256$ image to 5 kilobytes. 
 
The concept of structural similarity is covered. As are a number of related coding schemes, run--length coding, Huffman coding and entropy coding. 
Different quantization methods are discussed, and dithering is considered, but left out of the final scheme. 
A final compression scheme is described, and then compared to other compression schemes that were judged against it in a competition. 

\end{abstract}


\vspace*{\fill}
{
\begin{figure}[H]
\centering
\begin{subfigure}[t]{0.45\textwidth}
  \includegraphics[width=\linewidth]{../ImagesIII/lighhousecmp}
  \caption{Our Compression Scheme}
\end{subfigure}%
\hspace{2em}%
\begin{subfigure}[t]{0.45\textwidth}
  \includegraphics[width=\linewidth]{../ImagesIII/lighhouseraw}
  \caption{JPEG Compression}
\end{subfigure}
  \caption{Lighthouse images compressed to 5kB }
  \label{fig:frontpage}
\end{figure}
}
\vspace*{\fill}


\newpage
\tableofcontents
\listoffigures
\listoftables
\newpage
\pagenumbering{arabic}

\section{Introduction}

This report contains a description of various techniques used in the design of a compression scheme for images.

The compression scheme was applied to three different images, two that were supplied in advance: an image of a bridge, and another of some flamingos. An image of a window was provided at the last moment, so that the compression scheme could not be specifically optimised for this image. 
In addition to these, an image of a lighthouse was supplied for testing.
All images were grey--scale, and $256\times256$ pixels in size. The target size for the final compressed images was 5 kilobytes.

\section{Structural Similarity}

Rather than continue using RMS error as a measure of the severity of compression artefacts in an image, we decided to use a measure known as structural similarity.

The structural similarity of two images is defined according to equation \ref{eqn:ssim}.

\begin{equation}
\mathrm{SSIM}\left( x, y \right) = \frac{ \left( 2 \mu_x \mu_y + c_1 \right) \left( 2 \sigma_{xy} + c_2 \right) }{ \left( \mu_x^2 + \mu_y^2 + c_1 \right) \left( \sigma_x^2 + \sigma_y^2 + c_2 \right) }  \label{eqn:ssim}
\end{equation}

The decision to use structural similarity rather than mean squared error to assess image quality was due to inconsistencies between RMS error, and human judgement on image quality.
Structural similarity is designed to match human perception of image quality as closely as possible.

The code that was used to calculate structural similarity could also be used to create a map showing the parts of the image the errors occurred in. 
Maps of the errors in the final images are shown in Figure \ref{fig:errmaps}.

\section{Center Clipped Linear Quantizers}

The degree of data compression possible depends greatly on the number of samples that are compressed to zero, as this is the most common value.
Because of this, we investigated the use of a quantizer that mapped more samples onto zero.
This quantizer worked by widening the size of the zero step, according to a rise factor.
Quantization curves of this form are shown in Figure \ref{fig:quantizationfunctions}.

\begin{wraptable}{r}{8cm}
%\vspace{-1.5em}
\begin{centering}
\begin{tabular}{ c  c  c  c }
\toprule
\multirow{2}{*}{Scheme} & \multicolumn{3}{c}{$\texttt{rise1}/step$}\\
\cmidrule{2-4}
 & 0.5 & 1.0 & 1.5 \\ 
\midrule
\parbox{4cm}{\centering \vspace{0.2em}DCT\\ $\left(\textrm{block size}= 16\right)$\vspace{0.1em}} & 3.05 & 3.25 & 3.16\\
\cmidrule(l{4em}r{4em}){1-1}
\cmidrule{2-4}
\parbox{4cm}{\centering \vspace{0.2em}LBT\\ $\left(s=1.3, \textrm{block size}= 8\right)$\vspace{0.1em}} & 3.27 & 3.51 & 4.33\\
\cmidrule(l{4em}r{4em}){1-1}
\cmidrule{2-4}
\parbox{4cm}{\centering \vspace{0.2em}DWT\\ $\left(N=7, \textrm{varying step size}\right)$\vspace{0.1em}} & 3.16 & 3.35 & 3.25\\
\bottomrule
\end{tabular}
\caption{Compression ratios for different rise ratios}
\label{tab:risefactors}
\end{centering}
\end{wraptable}

A selection of the entropy measurements from previous reports were repeated with varying rise factors.
 The results of these measurements are shown in Table \ref{tab:risefactors}.
It is worth noting that when the value of $\texttt{rise1}/step$ is 0.5, then the quantization curve is uniform, and the quantization is the same as that used in previous reports.
The measurements in Table \ref{tab:risefactors} are the compression ratios for a matched RMS error.

Inspection of Table \ref{tab:risefactors} shows that for the transforms investigated, using a center clipped quantizer gives a higher compression ratio for the same RMS error.
Using a rise equal to the quantization step gave the best compression ratio for a fixed RMS error for all schemes investigated. 

\begin{figure}[H]
\centering
\ttfamily
\begin{gnuplot}[terminal=epslatex,terminaloptions=color]
set key left top
plot "foo.txt" using 1:2 title "rise=10" w lines lt 1 lw 3 linecolor rgb "yellow", "foo.txt" using 1:3 title "rise=20" w lines lt 1 lw 3 linecolor rgb "cyan", "foo.txt" using 1:4 title "rise=30" w lines lt 3 lw 3 linecolor rgb "magenta"  
\end{gnuplot}
\caption{Quantization functions with different rise values}
\label{fig:quantizationfunctions}
\end{figure}

\section{Run--Length Coding}
The compressed image data contains large numbers of long runs of zeros.
Because of this we can effectively run length encode the data.   
Two methods of run length encoding were attempted, the first one was traditional run length encoding, where the data is encoded as pairs of values, and the lengths runs.

In the second run length encoding scheme, data was encoded as pairs of values followed by the number of zeros that followed that value. 
This was an effective encoding scheme, because most of the data consisted of zeros, the probability that a value would be followed by a long run of zeros was very high.

Table \ref{tab:runlengthenc} shows an example of data compressed using the two different techniques.
This table demonstrates the advantage of the second scheme in situations where runs of zeros are much more common than any other run. 
\begin{table}[H]
\centering
\begin{tabular}{ r c c c c c c c c c c c c c c c c c c c c c c }
\toprule
Data & 1 & 0 & 0 & 0 & 3  & 2 & 0 & 0 & 0 & -1 & 0 & 0 & 1& 0& 0\\
\midrule
Traditional RLE & 1 & 1 & 0 & 3 & 1 & 2 & 1 & 0 & 3 & -1&  1 & 0 & 2 & 1 & 1 & 0 & 2\\ 
\midrule
RLE Zeros & 1 & 3 & 3 & 0 & 2 & 3 & -1 & 2 & 1 & 2\\
\bottomrule
\end{tabular} 
\caption{ An example of data run length encoded using each of the two run length methods} 
\label{tab:runlengthenc}
\end{table}

Tests on real data were also performed to confirm this.
Table \ref{tab:runlengthdata} shows the final file size in kilobytes when different forms of run length encoding were used. 
Note that the compression schemes used to generate this data used a fixed quantization step: the LBT and DWT compressed images did not have the same RMS error ore structural similarity, therefore it is not valid to compare them.
These compression schemes also used Huffman coding (described in Section \ref{sec:huffman}), and JPEG ordering (described in Section \ref{sec:entropy}).

This data clearly shows the advantage of run--length in general, and also the advantage of only encoding zeros as runs, as described in the second scheme.


\begin{table}[H]
\centering
\begin{tabular}{ r r c c c }
\toprule
 & & No Runlength Coding & Scheme One & Scheme Two \\ 
\cmidrule{3-5}
\multirow{2}{*}{Lighthouse} & DWT & 9.0800 & 4.0088 & 3.6345\\
 & LBT & 9.3352 & 3.8027 & 3.5154\\
\multirow{2}{*}{Bridge} & DWT & 9.0411 & 4.0087 & 3.4003\\
 & LBT & 9.4430 & 4.1853 & 3.7800\\
\multirow{2}{*}{Flamingo} & DWT & 9.5037 & 4.9196 & 4.3458\\
 & LBT & 9.8876 & 5.4330 & 4.8135\\
\bottomrule
\end{tabular} 
\caption{ Compressed image sizes achieved using different run--length schemes } 
\label{tab:runlengthdata}
\end{table}


\section{Huffman Coding}
\label{sec:huffman}

\begin{wrapfigure}{r}{10cm}
\centering
\vspace{-1.5em}
\begin{tikzpicture} 
\draw( 0, 3 ) rectangle +(4, 0.5);
\draw node[align=center] at (1.25,3.25){ A: \color{blue}$p=0.5$ };
\draw node[align=center] at (3.5,3.25){\color{red}\texttt{~~~0} };
\draw( 0, 2 ) rectangle +(4, 0.5);
\draw node[align=center] at (1.25,2.25){ B: \color{blue}$p=0.25$};
\draw node[align=center] at (3.5,2.25){\color{red}\texttt{~~10}};
\draw( 0, 1 ) rectangle +(4, 0.5);
\draw node[align=center] at (1.25,1.25){ C: \color{blue}$p=0.15$};
\draw node[align=center] at (3.5,1.25){\color{red}\texttt{~110}};
\draw( 0, 0 ) rectangle +(4, 0.5);
\draw node[align=center] at (1.25,0.25){ D: \color{blue}$p=0.1$ };
\draw node[align=center] at (3.5,0.25){\color{red}\texttt{~111}};

\draw (4, 0.25) -| +( 1, 0.5 );
\draw (4, 1.25) -| +( 1, -0.5 ) -- +( 2, -0.5);
\draw (4, 2.25) -| +( 2, -1.5 );
\draw (6, 1.5) -- +( 1, 0);
\draw (4, 3.25) -| +( 3, -1.75);
\draw (7, 2.375) -- +( 1, 0); 
 
\draw node[align=center] at (5.5,1){\color{blue}$0.25$ };
\draw node[align=center] at (5.5,0.5){\color{red}\texttt{11} };

\draw node[align=center] at (6.5,1.75){\color{blue}$0.5$ };
\draw node[align=center] at (6.5,1.25){\color{red}\texttt{1} };

\draw node[align=center] at (8.25,2.375){\color{blue}$1$ };
\end{tikzpicture}
\caption{Example of Huffman Coding}
\label{fig:huffmancoding}
\end{wrapfigure}

Huffman coding is a technique for generating run prefix free codes for a set of symbols, given the probability of each symbol.
Huffman coding can be used to encode data with known symbol probabilities with compression ratios close to the limit set by the Shannon entropy. 
However other coding techniques, such as arithmetic coding can achieve slightly higher compression ratios.

The JPEG file format uses Huffman encoding by standard, with an option to use Arithmetic coding. This part of the specification was set because when JPEG was designed, arithmetic coding was still covered by several patents. 

We decided to create a table of symbol probabilities for each image, and use this to generate an optimized Huffman \emph{dictionary}, including this with the compressed image data. This took well under half a kilobyte of data.

The decision to include the Huffman dictionary as part of the compressed image was made for simplicity as much as it was to achieve the highest possible compression level.
However, due to the small size of the dictionaries, it is probable that  

\section{Entropy Coding}
\label{sec:entropy}

\begin{wrapfigure}{l}{5cm}

\centering
\vspace{-1.5em}
\begin{tikzpicture} 

\foreach \x in {0,...,8}{
\draw (0, \x/2) -- ( 4, \x/2 );
\draw (\x/2, 0) -- ( \x/2, 4 );
}

\foreach \x in {0,...,7}{
\draw[thick, color=red] ( 0.25, \x/2 +0.25) -- ( 3.5 - \x/2+0.25, 3.75 );
}

\foreach \x in {1,...,7}{
\draw[thick, color=red] ( 3.75, 3.5-\x/2 +0.25) -- ( \x/2+0.25, 0.25 );
}

\foreach \x in {0,...,2}{
\draw[thick, color=red] ( 0.25, 0.75 + \x ) -- ( 0.25, 1.25 + \x );
\draw[thick, color=red] ( 3.75, 0.75 + \x ) -- ( 3.75, 1.25 + \x );
}

\foreach \x in {0,...,2}{
\draw[thick, color=red] ( 0.25 + \x, 0.25 ) -- ( 0.75 + \x, 0.25 );
}

\foreach \x in {1,...,3}{
\draw[thick, color=red] ( 0.25 + \x, 3.75 ) -- ( 0.75 + \x, 3.75 );
}

\draw[thick, color=red, >-] ( 0.25 , 3.75 ) -- ( 0.75, 3.75 );
\draw[thick, color=red, ->] ( 3.25, 0.25 ) -- ( 3.75, 0.25 );

\end{tikzpicture}
\caption{Entropy Coding}
\label{fig:jpegordering}
\end{wrapfigure}
Because we elected to use run--length coding, higher levels of compression could be achieved by minimizing the number of runs of zeros. 

We can achieve this via entropy coding: we rearrange image components in a zig--zag pattern as shown in Figure \ref{fig:jpegordering}.
We only applied this technique in the LBT compression scheme, as it was not applicable to the DWT scheme.

From inspection of Table \ref{tab:cmpcompare}, we can see that adding entropy coding as an extension to run length encoding slightly increases the image quality for an image compressed to a specified size. 

This indicates that entropy coding data compressed using a lapped bi--orthogonal transform based compression scheme is worthwhile.

These measurements also largely confirm the decision to set the rise in the quantizer equal to the step size, It should be noted that in the cases where this choice does not optimal structural similarity, another compression scheme gives an even better one, and this should be used instead. 

We only implemented entropy encoding for use on LBT coefficients, as DWT coefficients do not share the block structure.

\begin{table}[H]
\footnotesize
\centering
\begin{tabular}{ r c c c c c c c c c }
\toprule
Image & \multicolumn{3}{c}{Lighthouse} &  \multicolumn{3}{c}{Bridge} &\multicolumn{3}{c}{Flamingos} \\
\cmidrule(lr{0.25em}){2-4} 
\cmidrule(lr{0.25em}){5-7} 
\cmidrule(lr{0.25em}){8-10} 
Rise & 0.5 & 1.0 & 1.5 &  0.5 & 1.0 & 1.5 & 0.5 & 1.0 & 1.5 \\ 
\cmidrule(lr{0.25em}){2-4} 
\cmidrule(lr{0.25em}){5-7} 
\cmidrule(lr{0.25em}){8-10} 
DWT Scheme & 0.8378 & 0.8370 & 0.8270 & 0.7063 & 0.6979 & 0.6869 & 0.7644 & 0.7761 & 0.7647 \\  
LBT Scheme & 0.8384 & 0.8472 & 0.8459 & 0.7143 & 0.7096 & 0.6978 & 0.7503 & 0.7728 & 0.7700 \\
LBT \& Entropy Coding & 0.8420 &  0.8497 & 0.8492 & 0.7323 &  0.7298 & 0.7179 & 0.5729 & 0.7678 & 0.7780\\
\bottomrule
\end{tabular} 
\caption{ SSIM indices for different compression schemes and images} 
\label{tab:cmpcompare}

\end{table}

\section{Dithering}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{../ImagesIII/dither.png}
\caption{Lighthouse images compressed with various degrees of dither}
\end{figure} 
We investigated dithering of the quantized data, as a technique to improve the visual quality of the decompressed images. 

Dithering involves adding a small amount of noise to the quantized coefficients; this noise should be smaller than the quantization step.

This decreased the structural similarity between the compressed image and the original image. 
Similarly it increased the RMS error. 
This seems logical as, without knowledge of the non-quantized coefficients, no operations made to the quantized coefficients can logically decrease the error. 

Having implemented dithering, it was our opinion that it subtracted from the quality of the final images.
Given this, and the fact that dithering decreased the structural similarity, we decided not to use it in the final quantization scheme.
Dithering would have also made the quantization scheme non deterministic, this would have been undesirable, as it would have made measuring the characteristics of the compression scheme inconsistent between runs.

\section{The Final Compression Scheme}

We elected to use a scheme based on either the lapped bi-orthogonal transform, or the discrete wavelet transform, depending on the image. 
We decided against the use of the discrete cosine transform, as we did not want to generate an image with its characteristic blocky artefacts.

The lapped bi-orthogonal transform we used had an $8\times8$ block size, and 1.3 as the value of $s$, these were selected based on information in previous reports.
The discrete wavelet transform that we used, performed the DWT to the maximum possible seven levels.
The DWT quantization step varied with the level, and was chosen to produce a constant mean squared error at each level.

\subsection{Optimizing the Quantization for the Correct File Size}

In order to compress the image to the correct size, the final scheme involved using one of Matlab's functions: \texttt{fzero}, to find the quantization step (or the `\emph{base quantization step}' for the DWT) that gave the compressed data the correct size. 
The size that was optimized to was slightly smaller that the 5 kilobyte limit, this is to compensate for small errors in the calculation of the correct quantization step.  

\subsection{Selecting a Transform}
The correct quantization step was calculated for both quantized DWT and LBT coefficients.
Then, from this, the decompressed image was reconstructed and the compression scheme that gave the image with the greatest structural similarity to the uncompressed image. 
The final compressed image consisted of Huffman codes generated from the selected compressed coefficients, along with the Huffman dictionary, and a flag indicating that transform that was used. 
Figure \ref{fig:final} states the transform used to encode each image.

\section{Distribution of Work}

In the final compression scheme, I implemented Huffman encoding, plus the two transforms and the code to find the optimal quantization step.

My partner for this project, Alexander Wright, implemented the run length encoding algorithm, and the dithering code.

The code that chose the transform to use was developed together. 

\section{Conclusion}

\subsection{Comparison with Other Teams' Compression Schemes}  
Our images ranked relatively lowly in the competition compared to the images produced by other teams.
It is possible that we should have optimised the step size to use all of the data available, and the last 0.1 kilobytes would have made all the difference. 

Similarly we might have produced a higher quality image by using a standard Huffman dictionary and not including this in our final image.

Some of the most successful teams did things like include dithering, and filtering the decompressed image to make the compression artefacts less noticeable.

The winning team developed a large number of schemes, and asked a number of people to select a favourite, and used this to decide the scheme to use in their final algorithm.
Polling a large number of people in this manner would probably have been sensible, as we only had two opinions on the visual quality of each image 

\subsection{Overall Image Quality}

Although we ranked second to last in the competition, the images we produced were still relatively high quality, even when compared to a scheme such as JPEG, that is in very widespread use. 

This demonstrates the power of modern image compression techniques.

\newpage
\section{Final Images}

\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesIII/Xflam}
    \caption{Flamingos}
  \end{subfigure}\hspace{1px}%
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesIII/Xbridge}
    \caption{Bridge}
  \end{subfigure}\hspace{1px}%
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesIII/Xwin}
    \caption{Window}
  \end{subfigure}%
  \caption{The Original Uncompressed Images}
  \label{fig:originals}
\end{figure}

\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesIII/Zflam}
    \caption{Flamingos (DWT)}
  \end{subfigure}\hspace{1px}%
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesIII/Zbridge}
    \caption{Bridge (LBT)}
  \end{subfigure}\hspace{1px}%
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesIII/Zwin}
    \caption{Window (LBT)}
  \end{subfigure}%
  \caption{The Final Compressed Images}
  \label{fig:final}
\end{figure}

\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesIII/Mflam}
    \caption{Flamingos}
  \end{subfigure}\hspace{1px}%
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesIII/Mbridge}
    \caption{Bridge}
  \end{subfigure}\hspace{1px}%
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesIII/Mwin}
    \caption{Window}
  \end{subfigure}%
  \caption{Maps showing the error in the compressed images}
  \label{fig:errmaps}
\end{figure}

\end{document}

