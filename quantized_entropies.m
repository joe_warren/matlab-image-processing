function [Y0 Y1 Y2 Y3 X4] = quantized_entropies(X);

% create a pyramid X0 through to X4  

h = [1 2 1]/4;

step = 17;

X1 = rowdec( rowdec( X, h )', h )';
Y0 = X - rowint( rowint( X1, 2 .* h )', 2.*h )';

X2 = rowdec( rowdec( X1, h )', h )';
Y1 = X1 - rowint( rowint( X2, 2 .* h )', 2.*h )';

X3 = rowdec( rowdec( X2, h )', h )';
Y2 = X2 - rowint( rowint( X3, 2 .* h )', 2.*h )';

X4 = rowdec( rowdec( X3, h )', h )';
Y3 = X3 - rowint( rowint( X4, 2 .* h )', 2.*h )';

disp "-------------------------------------------------";
disp "OriginalImage";

x_bits = bpp( X ) * numel( X )
x_bits_quantized = bpp( quantise ( X,  step ) ) * numel( X )

disp "-------------------------------------------------";
disp "Pyramid1";

x1_bits = bpp( X1 )* numel( X1 )
x1_bits_quantized = bpp( quantise ( X1,  step ) ) * numel( X1 )

y0_bits = bpp( Y0 ) * numel( Y0 )
y0_bits_quantized = bpp( quantise ( Y0,  step ) ) * numel( Y0 )

Pyramid_1 = x1_bits_quantized + y0_bits_quantized 

disp "-------------------------------------------------";
disp "Pyramid2";

x2_bits = bpp( X2 )* numel( X2 )
x2_bits_quantized = bpp( quantise ( X2,  step ) ) * numel( X2 )

y1_bits = bpp( Y1 ) * numel( Y1 )
y1_bits_quantized = bpp( quantise ( Y1,  step ) ) * numel( Y1 )

Pyramid_2 = x2_bits_quantized + y0_bits_quantized + y1_bits_quantized 

disp "-------------------------------------------------";
disp "Pyramid3";

x3_bits = bpp( X3 )* numel( X3 )
x3_bits_quantized = bpp( quantise ( X3,  step ) ) * numel( X3 )

y2_bits = bpp( Y2) * numel( Y2 )
y2_bits_quantized = bpp( quantise ( Y2,  step ) ) * numel( Y2 )

Pyramid_3 = x3_bits_quantized + y0_bits_quantized + y1_bits_quantized + y2_bits_quantized

disp "-------------------------------------------------";
disp "Pyramid4";

x4_bits = bpp( X4 )* numel( X4 )
x4_bits_quantized = bpp( quantise ( X4,  step ) ) * numel( X4 )

y3_bits = bpp( Y3) * numel( Y3 )
y3_bits_quantized = bpp( quantise ( Y3,  step ) ) * numel( Y3 )

Pyramid_4 = x3_bits_quantized + y0_bits_quantized + y1_bits_quantized + y2_bits_quantized + y3_bits_quantized

disp "-------------------------------------------------";
disp "Pyramids";
x_bits_quantized
Pyramid_1 
Pyramid_2 
Pyramid_3 
Pyramid_4 
