function XQ = quantized_image( X )
%QUANTIZED_IMAGE Summary of this function goes here
%   Detailed explanation goes here

step = 17;

XQ = quantise( X, step );

draw( beside( X, XQ ) );

Diff = X-XQ;
RMS_Error = sqrt(sum( Diff(:).^2 )/numel(Diff))

end

