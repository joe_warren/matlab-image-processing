function [ X ] = nlevidwt( Y, n )
%NLEVIDWT Summary of this function goes here
%   Detailed explanation goes here


m = 256/(2^(n-1)); 

X = Y;

for i = 1:n
    t = 1:m;
    X(t, t) = idwt(X(t,t));
    m = m*2;
end

end

