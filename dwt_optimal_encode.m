function [ vlc, dict, stepbase, size ] = dwt_optimal_encode( X )
%DWT_OPTIMAL_ENCODE Summary of this function goes here
%   Detailed explanation goes here

rise = 1.0;

global image;
image = X;
global steprise;
steprise = rise;

stepbase = fzero( @dwt_size, 4500 );

[vlc, dict] = dwt_encode(X, stepbase, rise);

size = ( numel(vlc)/8 + 5 * (numel(dict)/2) ) / 1024;

end

