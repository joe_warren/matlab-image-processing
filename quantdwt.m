function [ Y ent ]  = quantdwt( X, n, q, rise )
%QUANTDWT Summary of this function goes here
%   Detailed explanation goes here


m = 256; 

Y = X;

ent = zeros( 3, n+1 );

for i = 1:n
    t = 1:m;
    Y(t, t) = dwt(Y(t,t));
    m = m/2;
    t = 1:m;
    Y(m + t, t) = quant1( Y(m + t, t), q( 1, i ) , q( 1, i ) *rise );
    Y(t, m + t) = quant1( Y(t, m + t), q( 2, i ), q( 2, i )*rise );
    Y(m + t, m + t) = quant1( Y(m + t, m + t), q( 3, i ), q( 3, i )*rise );
    
    ent( 1, i ) = bpp( Y(m + t, t) ) * numel( Y(m + t, t) );
    ent( 2, i ) = bpp( Y(t, m + t) ) * numel( Y(t, m + t) ) ;
    ent( 3, i ) = bpp( Y(m + t, m + t) ) * numel( Y(m + t, m + t) );
end

Y(t, t) = quant1( Y(t, t), q( 1, n+1 ), q( 1, n+1 )*rise );

ent(1, n+1 )= bpp( Y(t, t) ) * numel( Y(t, t) );

end

