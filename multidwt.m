function [ Y ] = multidwt( X )
%MULTIDWT Summary of this function goes here
%   Detailed explanation goes here

m = 256; 
Y = dwt(X);


while( m > 4 )
    m = m/2;
    t = 1:m;
    Y(t, t) = dwt(Y(t,t));
end

end

