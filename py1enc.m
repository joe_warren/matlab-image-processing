function [Y0, X1] = py1enc(X);

h = [1 2 1]/4;

X1 = rowdec( rowdec( X, h )', h )';
Y0 = X - rowint( rowint( X1, 2 .* h )', 2.*h )';
