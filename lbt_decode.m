function [ Z ] = dwt_decode( vlc, dict,basestep,rise )
%DWT_DECODE Summary of this function goes here
%   Detailed explanation goes here

n = 7;

HDEC = huffmandeco( vlc, dict );

RL = reshape( HDEC , numel(HDEC)/2 ,2  );

RLY = runlengthdec( RL );

Y = lbt_irearrange( RLY );

q = basestep;

Z = lbt_inverse( Y, q, rise );


end


