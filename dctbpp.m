function BPI = dctbpp( Yr, N )
%DCTBPP Summary of this function goes here
%   Detailed explanation goes here

BPI = zeros(N);

dimsub = 256 / N;

for i = 1:N
   for j = 1:N
      subimg = Yr( dimsub*(i-1)+1:dimsub*i, dimsub*(j-1)+1:dimsub*j );
      BPI( i, j ) = bpp(subimg) * numel(subimg); 
   end
end

end

