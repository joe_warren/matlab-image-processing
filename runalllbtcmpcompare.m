
load lighthouse

disp 'lighthouse'
disp 'lbt 0.5'
[s q] = lbt_compression_compare(X, 0.5 )

disp 'lighthouse'
disp 'lbt 1.0'
[s q] = lbt_compression_compare(X, 1.0 )

disp 'lighthouse'
disp 'lbt 1.5'
[s q] = lbt_compression_compare(X, 1.5 )


load bridge

disp 'bridge'
disp 'lbt 0.5'
[s q] = lbt_compression_compare(X, 0.5 )

disp 'bridge'
disp 'lbt 1.0'
[s q] = lbt_compression_compare(X, 1.0 )

disp 'bridge'
disp 'lbt 1.5'
[s q] = lbt_compression_compare(X, 1.5 )


load flamingo

disp 'flamingo'
disp 'lbt 0.5'
[s q] = lbt_compression_compare(X, 0.5 )

disp 'flamingo'
disp 'lbt 1.0'
[s q] = lbt_compression_compare(X, 1.0 )

disp 'flamingo'
disp 'lbt 1.5'
[s q] = lbt_compression_compare(X, 1.5 )

