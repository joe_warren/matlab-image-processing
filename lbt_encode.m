function [vlc, dict] = lbt_encode(X,basestep,rise);

n = 7   ;

set(0,'RecursionLimit',10000)

%q = ones( 3, n+1 ) * 3.95   ;
q = basestep;

Y = lbt_quantise( X, q, rise );

rl = runlength( lbt_rearrange( Y ) );

rl = rl(:);

symbols = unique( rl );
probs = histc( rl, symbols )/ sum( histc( rl, symbols ) );

dict = huffmandict(symbols,probs);
vlc = huffmanenco( rl, dict );