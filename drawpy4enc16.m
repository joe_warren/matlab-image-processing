function [Y0 Y1 Y2 Y3 X4] = drawpy4enc(X);

[Y0 Y1 Y2 Y3 X4] = py4enc16(X);

draw( beside( Y0, beside( Y1, beside( Y2, beside( Y3, X4-mean(X4(:)) ) ) ) ) );