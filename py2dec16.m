function [Z1 Z0] = py2dec( Y0, Y1, X2 )
%PY4DEC Summary of this function goes here
%   Detailed explanation goes here

h = [1 4 6 4 1]/16;

Z1 = Y1 + rowint( rowint( X2, h*2 )', h*2 )';

Z0 = Y0 + rowint( rowint( Z1, h*2 )', h*2 )';
end

