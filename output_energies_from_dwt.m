function E = output_energies_from_dwt( n )
%OUTPUT_ENERGIES FROM DWT Summary of this function goes here
%   Detailed explanation goes here

Y = zeros( 256, 256 );

E = zeros( 3, n+1 );

m = 256;

for i = 1:n
    m = m/2;
    
    Y(m + m/2, m/2 ) = 100;
    idwt = nlevidwt( Y, n );
    E( 1, i ) = sum( idwt(:).^2 );
    Y = zeros( 256, 256 );
    
    Y(m/2, m + m/2) = 100;
    idwt = nlevidwt( Y, n );
    E( 2, i ) = sum( idwt(:).^2 );
    Y = zeros( 256, 256 );
    
    Y(m + m/2, m + m/2) = 100;
    idwt = nlevidwt( Y, n );
    E( 3, i ) = sum( idwt(:).^2 );
    Y = zeros( 256, 256 );
    
end

m = m/2;

Y(m , m) = 100;
idwt = nlevidwt( Y, n );
E(1, n+1 ) = sum( idwt(:).^2 );

end

