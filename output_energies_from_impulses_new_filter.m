function m = output_energies_from_impulses()

L0 = zeros( 256 );
L1 = zeros ( 256 / 2 );
L2 = zeros( 256 / 4 );
L3 = zeros(256/8);
L4 = zeros( 256/16);

I0 = L0;
I1 = L1;
I2 = L2;
I3 = L3;
I4 = L4;

I0( 256/2, 256/2 ) = 100;
I1( 256/4, 256/4 ) = 100;
I2( 256/8, 256/8 ) = 100;
I3( 256/16, 256/16 ) = 100;
I4( 256/32, 256/32 ) = 100;

disp "--------------------"
disp "Pyramid4"

[Z3 Z2 Z1 Z0] = py4dec16( L0, L1, L2, L3, I4 );
Energy4thLayerImpulse = sum( Z0(:).^2 )

[Z3 Z2 Z1 Z0] = py4dec16( L0, L1, L2, I3, L4 );
Energy3rdLayerImpulse = sum( Z0(:).^2 )

[Z3 Z2 Z1 Z0] = py4dec16( L0, L1, I2, L3, L4 );
Energy2ndLayerImpulse = sum( Z0(:).^2 )

[Z3 Z2 Z1 Z0] = py4dec16( L0, I1, L2, L3, L4 );
Energy1stLayerImpulse = sum( Z0(:).^2 )

[Z3 Z2 Z1 Z0] = py4dec16( I0, L1, L2, L3, L4 );
Energy0thLayerImpulse = sum( Z0(:).^2 )

m=0;
end

