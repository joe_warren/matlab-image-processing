function Z = dct_quantise( X )
%DCT_QUANTISE Summary of this function goes here
%   Detailed explanation goes here

step = 10.22 ;
N=16;

C = dct_ii(N);

Y = colxfm( colxfm(X-128, C)', C )';

Yq = quantise( Y, step, step * 1.5 );


BPP = dctbpp(Yq, N)
BPI = sum(BPP(:))

Z = colxfm( colxfm(Yq', C')',C');

diff1 = Z - X + 128 ;
RMS_ERROR = sqrt(sum( diff1(:).^2 )/numel(diff1))
end

