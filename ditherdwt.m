function Yd=ditherdwt(Y,n,step)
%m=256/(2^n);
m=size(Y,1)/2;
Yq=zeros(m*2);


t=1:m;
%Yq(t,t)=quantise(Y(t,t),step(1,n+1));
if n==0
    Yd=Y+(rand(2*m)-.5)*step(1,1);
else
    Yd(t,t)=ditherdwt(Y(t,t),n-1,step(:,[2:end]));
    Yd(t,t+m)=Y(t,t+m)+(rand(m)-.5)*step(1,1);
    Yd(t+m,t)=Y(t+m,t)+(rand(m)-.5)*step(2,1);
     Yd(t+m,t+m)=Y(t+m,t+m)+(rand(m)-.5)*step(3,1);
    %size(step)
    %size(Y)
    %t
    %[~,~]=quantdwt(Y(t,t),n-1,step(:,[2:end]));
    %size(ent)
    %size(entp)
end
end