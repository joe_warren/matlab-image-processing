function [vlc dict stepbase scheme] = encode( filename )
%ENCODE Summary of this function goes here
%   Detailed explanation goes here
load( filename );

[vlc dict stepbase scheme] = optimal_encode(X);

%treat each bit as a zero length codeword so vlctest passes
vlc = [vlc' ones(numel(vlc), 1)];

output = [filename( 1:5 )  'cmp.mat'];

save( output , 'vlc', 'dict', 'stepbase', 'scheme' );

end

