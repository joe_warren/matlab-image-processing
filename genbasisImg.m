function [ B, Xp ] = genbasisImg( X )
%GENBASISIMG Summary of this function goes here
%   Detailed explanation goes here

N=8;

s = 1.3;

C = dct_ii(N);

[Pf Pr] = pot_ii(N, s);

I = 256;
t= [(1+N/2):(I-N/2)];

bases = [zeros(1,8); Pf';zeros(1,8)];
B = 255*bases(:)*bases(:)'

Xp = X-128;
Xp(t,:) = colxfm( Xp(t,:), Pf);
Xp(:,t) = colxfm( Xp(:,t)', Pf)';


end

