function [ Zp ] = lbt_inverse( Yq, step, rise )
%LBT_INVERSE Summary of this function goes here
%   Detailed explanation goes here

N= 8;

s = 1.3;

C = dct_ii(N);

[Pf Pr] = pot_ii(N, s);

I = 256;
t= [(1+N/2):(I-N/2)];

Yq = quant2( Yq, step, step*rise );
Z = colxfm( colxfm(Yq', C')',C');

Zp = Z;
Zp(:, t) = colxfm(Zp(:,t)', Pr')'; 
Zp(t, :) = colxfm(Zp(t,:) , Pr'); 

end

