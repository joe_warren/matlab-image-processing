function [Z0] = py1dec( Y0, X1 )
%PY4DEC Summary of this function goes here
%   Detailed explanation goes here

h = [1 2 1]/4;
Z0 = Y0 + rowint( rowint( X1, h*2 )', h*2 )';
end
