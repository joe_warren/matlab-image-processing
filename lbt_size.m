function size = lbt_size(stepbase)
%DWT_SIZE Summary of this function goes here
%   Detailed explanation goes here

global image;
global steprise;
[vlc, dict] = lbt_encode(image , stepbase, steprise);

size = ( numel(vlc)/8 + 5 * (numel(dict)/2) ) / 1024- 4.9;

end

