function [Z3 Z2 Z1 Z0] = py4dec( Y0, Y1, Y2, Y3, X4 )
%PY4DEC Summary of this function goes here
%   Detailed explanation goes here

h = [1 4 6 4 1]/16;

Z3 = Y3 + rowint( rowint( X4, h*2 )', h*2 )';

Z2 = Y2 + rowint( rowint( Z3, h*2 )', h*2 )';

Z1 = Y1 + rowint( rowint( Z2, h*2 )', h*2 )';

Z0 = Y0 + rowint( rowint( Z1, h*2 )', h*2 )';
end

