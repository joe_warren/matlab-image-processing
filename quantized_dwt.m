function [Z] = quantized_dwt(X);

n = 7   ;

%q = ones( 3, n+1 ) * 3.95   ;
q = 1./sqrt( output_energies_from_dwt( n ) ) * 1173;

[ Y ent ] = quantdwt( X - 128, n, q );

Z = nlevidwt( Y, n );

Diff = Z - X + 128;

BPI = sum( ent(:) )

RMS_Error = sqrt(sum( Diff(:).^2 )/numel(Diff))
