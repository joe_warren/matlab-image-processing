function [vlc, dict] = dwt_encode(X,basestep,rise);

n = 7   ;

set(0,'RecursionLimit',10000)

%q = ones( 3, n+1 ) * 3.95   ;
q = 1./sqrt( output_energies_from_dwt( n ) ) * basestep;

[ Y ent ] = quantdwt( X - 128, n, q, rise );

rl = runlength( Y(:)' );

rl = rl(:);

symbols = unique( rl );
probs = histc( rl, symbols )/ sum( histc( rl, symbols ) );

dict = huffmandict(symbols,probs);
vlc = huffmanenco( rl, dict );