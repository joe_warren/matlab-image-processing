\documentclass[a4paper,10pt]{article}

%\usepackage{a4wide}
\usepackage[cm]{fullpage}
\usepackage{amsmath}

\usepackage{array}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{listings}
\usepackage{fixltx2e}
\usepackage[justification=centering]{caption}
\usepackage{subcaption}
\usepackage{emp}
\usepackage{multirow}

\usepackage[runin]{abstract}


\usepackage{tikz}
\usepackage{gensymb}

\renewcommand{\abstractname}{Summary: } 
\renewcommand{\absnamepos}{empty}

\begin{document}

\pagenumbering{roman}

\title{ SF2 :: Image Processing\\Second Interim Report }
\author{Joseph Warren ( jw718 ) \\ Churchill College}
\date{22/5/14}

\maketitle


\begin{abstract}
This report will analyse compression schemes based on:
\begin{itemize}
\item The discrete cosine transform
\item The lapped bi-orthogonal transform
\item The discrete wavelet transform, utilising binary wavelet trees
\end{itemize}

The effect of varying the block size of the discrete cosine transform on the compression ratio is considered and this is applied to both the discrete cosine transform and the lapped bi-orthogonal transform. 
The transforms are investigated independently of their use in compression schemes.
The discrete wavelet transform  is used in compression schemes based on both a single quantization step size, and a quantization step size that varies with the layer of the binary wavelet tree. 
Different images are compressed using the discrete wavelet transform, and the degree  of compression achieved is discussed.


Compression is compared using compression ratios baselined against a directly quantized image. 

\end{abstract}


\vspace*{\fill}
{

\begin{figure}[H]
\centering
\begin{tikzpicture} 

\foreach \x in {0,...,2}{
\begin{scope}[shift={(\x*1.75,\x)}]
\draw[thick, ->] (0, 0) -- ( 0.5, 0 );
\draw[thick](0.5,-0.25) rectangle +(0.5, 0.5);
\draw node[align=center] at (0.75,0){$H_1$};
\draw[thick, ->] (1, 0) -- ( 1.25, 0 );
\draw node[align=center] at (1.5,0){$\downarrow^2$};
\draw[thick](1.25,-0.25) rectangle +(0.5, 0.5);

\draw[thick, -] (0.25, 0) -- ( 0.25, 1 );
\draw[thick, ->] (0.25, 1) -- ( 0.5, 1 );
\draw[thick](0.5, 0.75) rectangle +(0.5, 0.5);
\draw node[align=center] at (0.75,1){$H_0$};
\draw[thick, ->] (1, 1) -- ( 1.25, 1 );
\draw node[align=center] at (1.5,1){$\downarrow^2$};
\draw[thick](1.25, 0.75) rectangle +(0.5, 0.5);
\end{scope}
}


\draw node[align=center] at (3.15 ,4){Forward Transform};

\draw node[align=center] at (10.75 ,4){Inverse Transform};

\foreach \x in {0,...,2}{
\begin{scope}[shift={(12-\x*2.25,\x)}]

\draw node[align=center] at (0.25,1){$\uparrow_2$};
\draw[thick](0, 0.75) rectangle +(0.5, 0.5);
\draw[thick, ->] (0.5, 1) -- +( 0.25, 0 );
\draw node[align=center] at (1,1){$G_0$};
\draw[thick](0.75, 0.75) rectangle +(0.5, 0.5);
\draw[thick, -] (1.25, 1) -- +( 0.5, 0 );
\draw[thick, ->] (1.75, 1) -- +( 0, -0.75 );

\draw node[align=center] at (0.25,0){$\uparrow_2$};
\draw[thick](0, -0.25) rectangle +(0.5, 0.5);
\draw[thick, ->] (0.5, 0) -- +( 0.25, 0 );
\draw node[align=center] at (1,0){$G_1$};
\draw[thick](0.75, -0.25) rectangle +(0.5, 0.5);
\draw[thick, ->] (1.25, 0) -- +( 0.25, 0 );
\draw[thick]( 1.75, 0 ) circle ( 0.25);
\draw node[align=center] at( 1.75, 0 ) {$+$};
\draw[thick, ->] (2, 0) -- +( 0.25, 0 );

\end{scope}
}


\draw[dashed]( 6.375 ,-0.5 ) -- +( 0, 0.5 );

\draw[thick, ->](  1.75,0 ) -- +( 10.25, 0 );
\draw node[align=center] at( 6.375 , 0.20 ) {\footnotesize\textit{Level 1 Coefficients}};

\draw[dashed]( 6.375 , 0.5 ) -- +( 0, 0.5 );

\draw[thick, ->](  3.5,1 ) -- +( 6.25, 0 );
\draw node[align=center] at( 6.375 , 1.20 ) {\footnotesize\textit{Level 2 Coefficients}};
\draw[dashed]( 6.375 , 1.5 ) -- +( 0, 0.5 );

\draw[thick, ->](  5.25,2 ) -- +( 2.25, 0 );
\draw[thick, ->](  5.25,3 ) -- +( 2.25, 0 );

\draw[dashed]( 6.375 , 3 ) -- +( 0, 1.5 );

\draw node[text width=2cm, align=center] at( 6.375 , 2.5 ) {\footnotesize\textit{Level 3\\Coefficients}};

\end{tikzpicture}
\caption{A three level binary discrete wavelet transform and its inverse}
\label{fig:threelevelbdwt}
\end{figure}

}
\vspace*{\fill}



\newpage
\pagenumbering{arabic}

\section{The Discrete Cosine Transform}

Figure \ref{fig:DCT8} shows a regrouped version of the $8\times8$ block Discrete Cosine Transform (DCT) of the lighthouse image.
This image is grouped so higher frequencies are shown towards the bottom right, and lower frequencies are shown top left.
As the frequency increases, the sub-image energy decreases.

The basis function of the DCT used is shown in Figure \ref{fig:DCTbasis}.
The patterns in this image represent the different frequencies expressed in each block of the DCT.
Taking the DCT of a block transforms it into the space of these patterns.
The sub images of Figure \ref{fig:DCT8} represent the component of the blocks in direction of the pattern in the corresponding position of Figure \ref{fig:DCTbasis}. 

\subsection{Quantization and Coding Efficiency}

\begin{wraptable}{r}{5cm}
\begin{centering}
\begin{tabular}{ c | c  }
\texttt{dctbpp(Yr, N)} & \texttt{bpp(Yr)}\\
\hline
$9.7468 \times 10^4$ & $1.0963 \times 10^ 5 $\\
\end{tabular}
\caption{Bits in \texttt{Yr} calculated using different methods}
\label{tab:bppdct}
\end{centering}
\end{wraptable}

A function: \texttt{dctbpp(Yr, N)} was written to calculate the total number of bits for a regrouped image, by using \texttt{bpp} on each sub image.
The DCT of the lighthouse image was quantized with a step size of 17, and regrouped to give an image \texttt{Y}.
The total number of bits in \texttt{Yr} was calculated using both dctbpp, and bpp, this gave the results shown in Table \ref{tab:bppdct}. 

\begin{wraptable}{r}{9cm}
\begin{centering}
\begin{tabular}{ c | c | c | c }
& X Quantized &  \multicolumn{2}{c}{Y Quantized}\\
\hline
Step & 17 & 17 & 24.1545 \\
RMS Error & 4.9340 & 3.7572 & 4.9340 \\
Compression Ratio & 1.0 & 2.347 & 2.906
\end{tabular}
\caption{ Compression ratios for a DCT block size of $8 \times 8$}
\label{tab:dctrmserr}
\end{centering}
\end{wraptable}

\texttt{Yq} was used to compute an output image: \texttt{Z}.
The RMS error in Z could then be calculated.
The RMS error and bits per image of \texttt{Yr} was calculated for a quantization step of 17, and a quantization step chosen to match the RMS error produced by quantizing \texttt{X} with a step size of 17.    
Table \ref{tab:dctrmserr} shows the results of this.
In Figure \ref{fig:dctquant}, DCT quantized images are shown alongside a directly quantized image with the same RMS error.
Sub-figure \ref{fig:dctN8} shows the DCT with the block size of 8.    

The error in the directly quantized image is much more  visually obvious than the error in the DCT quantized image.
The DCT quantized image has visible \emph{blocky} artefacts, in contrast to the bands in the directly quantized image. 
Near to sharp transitions in the image (such as the edge of the roof) there is a reasonable amount of high frequency noise. 

\subsection{Alternate Transform Sizes}

\begin{wraptable}{r}{10cm}
\begin{centering}
\begin{tabular}{ c | c | c | c | c | c | c }
N & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{8} & \multicolumn{2}{c}{16}\\
\hline
Step & 17 & 24.34 & 17 & 24.16 & 17 & 22.74 \\
RMS Error & 3.74 & 4.93 & 3.76 & 4.93 & 3.90 & 4.93\\ 
Compression Ratio & 2.02 & 2.46 & 2.35 & 2.906 & 2.51 & 3.05\\
\end{tabular}
\caption{Compression ratios for varying DCT block sizes}
\label{tab:dctvarn}
\end{centering}
\end{wraptable}

The measurements from the previous section were repeated using $ 4\times4$ and $16\times16$ DCTs.
These measurements are shown in Table \ref{tab:dctvarn}.
Figure \ref{fig:dctquant} shows images compressed using different sizes of DCT block with matched errors.

Inspection of Figure \ref{fig:dctquantdtl} shows that the \emph{blocky} artefacts in each N$\times$N DCT compressed image are N$\times$N pixels in size, hence they are smallest when N $= 4$ and largest when N $= 16$.
Visually, the \emph{blocky} artefacts are more noticeable when N $= 4$ and most subtle when N $= 16$. 
However since the artefacts are larger when N $= 16$, the noise around sharp transitions covers a larger area.

N $=16$ has the highest compression ratio, for a matched RMS error.
Subjectively, N $= 8$ and N $=16$ look very similar, and N $=8$ gives the best quality image, as it strikes a balance between having less noticeable artefacts, and having only a small amount of noise around transitions.

An image with a lot of sharp transitions might look better when a smaller block size was used, while an image that consisted mostly of low frequency information would probably look better with a larger block size. 
If a sharp transition in an image lined up exactly along a boundary between blocks, this would look significantly better than if the transition appeared within a block.

If we calculate \texttt{dctbpp(Yr, 256)} this will return zero for all \texttt{Yr}.
This is because the entropy of a $1\times1$ image is zero.
This is obviously not the case, as the idea that an image could be encoded using zero bits is absurd.
In reality information is required to describe how each block is encoded, as N $\Rightarrow 256$ the amount of this information required increases dramatically.

\section{The Lapped Bi-Orthogonal Transform}

The lapped bi-orthogonal transform(LBT) was implemented based on an $8\times8$ DCT.
The scaling factor $s$ used in the transform was varied, and the quantization step was set so that the RMS error in the reformed image was 4.93.
 
The results from this are shown in Table \ref{tab:LBTs}, and a selection of the restored images produced are included in Figure \ref{fig:LBTs}.


\begin{wraptable}{r}{15cm}
\begin{centering}
\begin{tabular}{ c | c | c | c | c | c | c | c | c | c | c }
$s$ & 1 & 1.1 & 1.2 & 1.25 & 1.3 & 1.35 & $\sqrt{2}$ & 1.6 & 1.8 & 2.0\\
\hline
Step& 23.80 & 24.69 & 25.38 & 25.67 & 25.97 & 26.20 & 26.47 & 26.94 & 27.11 & 26.99\\ 
\parbox{2cm}{\centering Compression Ratio} & 2.98 & 3.03 & 3.05 & 3.056 & 3.061 & 3.055 & 3.05 & 2.98 & 2.87 & 2.74\\
\end{tabular}
\caption{Compression ratios for varying scaling factors}
\label{tab:LBTs}
\end{centering}
\end{wraptable}

The best value for $s$ found was 1.3.

Examination of Figures \ref{fig:LBTs} and \ref{fig:LBTsdtl} shows that with a larger value of $s$, the noisy artefacts that appear around sharp transitions in the image are greater.
However larger values of $s$ also lead to sharper transitions, and choosing a small value of $s$ makes the image look blurry.
None of the images have distinct \emph{blocky} artefacts, like the DCT did, as the purpose of the LBT is to reduce these.

\subsection{ Basis Functions and Pre-Filtered Images }

Basis functions of the LBT are shown in Figure \ref{fig:LBTbasis}, alongside pre-filtered images.
Increasing the scaling factor makes the pre-filtered image look significantly \emph{blockier}.
While the pre-filtered image with $s = 1$ has noticeable block artefacts,  they are not nearly as prominent as in the pre-filtered image for $s = 2$ 

\subsection{ Alternate Transform Sizes }

\begin{wraptable}{r}{8cm}
\begin{centering}
\begin{tabular}{ c | c | c | c }
N & 4 & 8 & 16\\
\hline
Step & 28.376 & 25.978 & 23.1956 \\
Compression Ratio & 2.994 & 3.061 & 3.054  \\
\end{tabular}
\caption{Compression ratios for varying LBT block sizes}
\label{tab:LBTvarN}
\end{centering}
\end{wraptable}

The LBT was carried out using a range of different block sizes, $s =  1.3$ was used as the scaling factor and the quantization step was set so the final RMS error was 4.934. 
These results are shown in Table \ref{tab:LBTvarN}, and the images produced are shown in Figure \ref{fig:LBTvarN}.

Using a block size of $8\times8$ gives the best compression ratio.
Examination of Figure \ref{fig:LBTvarN} shows that the larger the block size, the larger the bands of noise that surround sharp transitions in the image are, such as the edge of the lighthouse.
However a larger block size makes flat areas of the image, such as the sky look smoother, whereas in the N $ = 4$ image the sky looks slightly pixelated. 


\section{The Discrete Wavelet Transform}

Figure \ref{fig:dwtUV} contains the images \texttt{U} and \texttt{V}, obtained via the Matlab commands: \texttt{U = rowdec(X,h1);} and \texttt{V = rowdec2(X, h2);}. 
Where \texttt{h1} and \texttt{h2} are two LeGall filters.
There is significantly more energy in \texttt{U} that there is in \texttt{V}.

By filtering the rows of \texttt{U} and \texttt{V} we can produce the image \texttt{[UU VU; UV VV]}. 
This is shown (broken up into components) in Figure \ref{fig:dwtdouble}. 
\texttt{UU} is the image, low pass filtered, and decimated, this essentially selects low frequency information. 
\texttt{UV} selects horizontal edges, and \texttt{VU} selects vertical edges.
\texttt{VV} selects for variation in both horizontal and vertical directions, such as the roof in the sample image, as well as the top edge of the fence. 

When displaying \texttt{VV} in Figure \ref{fig:dwtdouble}, we multiply it by a constant factor.
The reason that this is required, is that since high pass filtering removes most of the energy, and \texttt{VV} has been high pass filtered twice, it has very low energy. Therefore we have to amplify it in order to display it.

Iterative code to split the image in a binary wavelet tree was produced.
The output produced by applying an N level binary wavelet tree to the lighthouse image is shown in Figure \ref{fig:bwt}.

\subsection{Quantization and Coding Efficiency}

\begin{wraptable}{r}{10cm}
\begin{centering}

$10^6 \times \left[\begin{array}{ c c  c  c  c  c }
0.0431, & 0.1014, & 0.3410, & 1.3009, & 5.1408, & 4.5556\\
0.0431, & 0.1014, & 0.3410, & 1.3009, & 5.1408, & 0 \\
0.0827, & 0.1360, & 0.4024, & 1.4815, & 5.8013, & 0 \\ 
\end{array}\right]$

\caption{Energy produced in the output image by an impulse in a sub-image of the binary wavelet tree }
\label{tab:energies}
\end{centering}
\end{wraptable}

Two compression schemes were implemented: an equal step size scheme and an equal energy compression scheme.
The equal energy quantization scheme required calculating the amount of energy contributed by an impulse in each sub-image in the binary wavelet tree. The energies for a 5 level tree are shown in Table \ref{tab:energies} 
Quantization levels were selected to give an RMS error of 4.934.
The measurements recorded are shown in Table \ref{tab:bwtNquant}.  

From this data, we can see that the compression scheme with the varying step size outperforms the scheme with the fixed step size. 
In the varying step size scheme, the compression ratio increases monotonically with the number of layers, so that 7 layers (the maximum possible) gives the optimal compression ratio. 

Figures \ref{fig:constantbwtcompression} and \ref{fig:varyingbwtcompression} show a sample of images compressed using these schemes.  

\begin{wraptable}{r}{14.5cm}
\begin{centering}
\begin{tabular}{ c | c | c | c | c | c | c | c | c}
 \multicolumn{2}{c |}{N} & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
\hline
\multirow{2}{*}{Constant Step Size} & Step &
 12.45 & 9.54 & 7.82 & 6.67 & 5.74 & 5.32 & 4.73\\
 & Compression Ratio & 2.20 & 2.81 & 2.73 & 2.52 & 2.29 & 2.20 & 2.06 \\
\hline
\multirow{2}{*}{Varying Step Size} & Step Multiplier &
2276 & 2533 & 2553 & 2553 & 2555 & 2555 & 2554 \\
 & Compression Ratio & 2.16 & 2.90 & 3.10 & 3.14 & 3.15 & 3.16 & 3.16\\
\end{tabular}
\caption{ Compression ratios for different binary wavelet tree compression schemes }
\label{tab:bwtNquant}
\end{centering}
\end{wraptable}

In Figures \ref{fig:constantbwtcompression} and \ref{fig:varyingbwtcompression}, at low numbers of levels, the compressed image appears slightly pixelated in smooth areas, such as the sky. 
 When more levels are used, smooth areas appear blurrier.
In all of the compressed images areas close to sharp transitions have a small amount of noise. 

Along with the lighthouse image, the measurements were repeated on an image of a bridge (shown in Figure \ref{fig:varyingbwtbridge}), this image includes a lot of fine detail.
This was done using the step sizes from the lighthouse image(for the varying scheme only), and with new step sizes, calculated to give an RMS error of 4.934. 
These results are shown in Tables \ref{tab:bridgeoldstep}, \ref{tab:bridgenewstep} and \ref{tab:bridgefixedstep}. 

\begin{wraptable}{r}{11cm}
\begin{centering}
\begin{tabular}{ c | c | c | c | c | c | c | c}
N & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
\hline
Step Multiplier & 2276 & 2533 & 2553 & 2553 & 2555 & 2555 & 2554 \\
Compression Ratio & 1.91 & 2.27 & 2.34 & 2.36 & 2.37 & 2.37 & 2.37\\
RMS Error & 5.75 & 6.55 & 6.60 & 6.61 & 6.61 & 6.61 & 6.61\\
\end{tabular}
\caption{Compression ratios and RMS errors from the bridge image, when compressed using the varying step schemes devised for the lighthouse image}
\label{tab:bridgeoldstep}
\end{centering}
\end{wraptable}

The compression ratios achieved with the bridge image are significantly lower than those with the lighthouse image. 
When the step sizes calculated for the lighthouse image are used to compress the bridge image, not only is the compression ratio lower, but the RMS error is also consistently higher.

\begin{wraptable}{r}{11cm}
\begin{centering}
\begin{tabular}{ c | c | c | c | c | c | c | c}
N & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
\hline
Step Multiplier & 1831 & 1833 & 1831 & 1831 & 1830 & 1830 & 1830 \\
Compression Ratio & 1.62 & 1.81 & 1.84 & 1.85 & 1.85 & 1.86 & 1.86\\
\end{tabular}
\caption{Compression ratios when the varying step scheme was used on the bridge image with step size chosen for a fixed RMS error}
\label{tab:bridgenewstep}
\end{centering}
\end{wraptable}

Like the lighthouse image, with the bridge image, for a fixed RMS error, increasing the number of levels always increased the compression ratio.

Comparison of Tables \ref{tab:bridgenewstep} and \ref{tab:bridgefixedstep} shows that the varying step size scheme still outperforms the fixed step size scheme on the bridge image. 


Figure \ref{fig:varyingbwtbridge} shows the bridge image, compressed using a varying step size scheme, and a range of different levels. 

In this figure, we can see that when a low number of levels are used, artefacts appear that make the image look like it is pixelated. 

\begin{wraptable}{r}{11cm}
\begin{centering}
\begin{tabular}{ c | c | c | c | c | c | c | c}
N & 1 & 2 & 3 & 4 & 5 & 6 & 7\\
\hline
Step Size & 8.85 & 6.99 & 6.15 & 5.52 & 5.05 & 4.60 & 3.95 \\
Compression Ratio & 1.60 & 1.74 & 1.70 & 1.61 & 1.53 & 1.45 & 1.33 \\
\end{tabular}
\caption{Compression ratios when a constant step size scheme was used on the bridge image to produce a fixed RMS error}
\label{tab:bridgefixedstep}
\end{centering}
\end{wraptable}

When a higher number of levels are used, the pixelation like artefacts are no longer present.
However the branches in the foreground, that are high frequency components of the image, appear blurred. 
Also when a high number of levels are used, an amount of noise is added to the image, this is only obvious when directly comparing the compressed and uncompressed images.



\newpage
\appendix
\pagenumbering{Roman}
\section{Figures}

\begin{figure}[H]
\centering
\minipage[t]{0.25\textwidth}
  \centering
  \includegraphics[width=\linewidth]{../ImagesII/DCT_8_Y}
  \caption{DCT transformed image, regrouped so equal frequency components are in blocks}
  \label{fig:DCT8}
\endminipage
\hspace{2em}
\minipage[t]{0.25\textwidth}
  \centering
  \includegraphics[width=\linewidth]{../ImagesII/DCT_basis}
  \caption{Basis functions of the DCT}
  \label{fig:DCTbasis}
\endminipage
\end{figure}

\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../Images/QuantizedPyramids/X}
    \caption{Directly Quantized}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/reformedXmatchedRMS_size4}
    \caption{DCT-Compressed\\N = 4}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/reformedXmatchedRMSerr}
    \caption{DCT-Compressed\\N = 8}
    \label{fig:dctN8}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/reformedXmatchedRMS_size16}
    \caption{DCT-Compressed\\N = 16}
  \end{subfigure}%
  \caption{Images compressed with the same RMS error }
  \label{fig:dctquant}
\end{figure}

\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/Xquantdtl}
    \caption{Directly Quantized}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/reformedXmatchedRMS_size4_dtl}
    \caption{DCT-Compressed\\N = 4}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/reformedXmatchedRMSerr_dtl}
    \caption{DCT-Compressed\\N = 8}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/reformedXmatchedRMS_size16_dtl}
    \caption{DCT-Compressed\\N = 16}
  \end{subfigure}%
  \caption{Details from Figure \ref{fig:dctquant} }
  \label{fig:dctquantdtl}
\end{figure}


\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/sisone}
    \caption{$s=1$}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/sis13}
    \caption{$s=1.3$}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/sis16}
    \caption{$s=1.6$}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/sistwo}
    \caption{$s=2$}
  \end{subfigure}%
  \caption{LBT compressed images with varying scaling factors  }
  \label{fig:LBTs}
\end{figure}


\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/sisonedtl}
    \caption{$s=1$}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/sis13dtl}
    \caption{$s=1.3$}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/sis16dtl}
    \caption{$s=1.6$}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/sistwodtl}
    \caption{$s=2$}
  \end{subfigure}%
  \caption{Details from Figure \ref{fig:LBTs}  }
  \label{fig:LBTsdtl}
\end{figure}


\begin{figure}[H]
\centering
  \minipage[t]{0.2\textwidth}
  \begin{subfigure}[t]{\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/basis/10}
    \caption{Basis Function\\$s=1$}
  \end{subfigure}
  \begin{subfigure}[t]{\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/basis/10prefiltered}
    \caption{\texttt{Xp}: $s=1$}
  \end{subfigure}%
  \endminipage\hspace{1em}%
  \minipage[t]{0.2\textwidth}
  \begin{subfigure}[t]{\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/basis/13}
    \caption{Basis Function\\$s=1.3$}
  \end{subfigure}
  \begin{subfigure}[t]{\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/basis/13prefiltered}
    \caption{\texttt{Xp}: $s=1.3$}
  \end{subfigure}%
  \endminipage\hspace{1em}%
  \minipage[t]{0.2\textwidth}
  \begin{subfigure}[t]{\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/basis/20}
    \caption{Basis Function\\$s=2$}
  \end{subfigure}
  \begin{subfigure}[t]{\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/basis/20prefiltered}
    \caption{\texttt{Xp}: $s=2$}
  \end{subfigure}%
  \endminipage
  \caption{ LBT bases and pre-filtered images   }
  \label{fig:LBTbasis}
\end{figure}


\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/blocksize/4}
    \caption{N $ = 4$}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/blocksize/8}
    \caption{N $= 8$}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/lbt/blocksize/16}
    \caption{ N $=16$}
  \end{subfigure}\hspace{1em}%
  \caption{LBT compressed images with varying block sizes}
  \label{fig:LBTvarN}
\end{figure}

\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.125\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/multidwt/256}
    \caption{N = 1}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.125\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/multidwt/128}
    \caption{N = 2}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.125\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/multidwt/64}
    \caption{N = 3}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.125\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/multidwt/32}
    \caption{N = 4}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.125\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/multidwt/16}
    \caption{N = 5}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.125\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/multidwt/8}
    \caption{N = 6}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.125\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/multidwt/4}
    \caption{N = 7}
  \end{subfigure}\hspace{1em}%
  \caption{Images obtained from an N Level binary wavelet tree}
  \label{fig:bwt}
\end{figure}

\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=0.75\textwidth]{../ImagesII/dwt/u}
    \caption{\texttt{U = rowdec(X,h1)}}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.2\textwidth}
    \centering
    \includegraphics[height=0.75\textwidth]{../ImagesII/dwt/v}
    \caption{\texttt{V = rowdec2(X, h2)}}
  \end{subfigure}\hspace{1em}%
  \caption{Lowpass and highpass images obtained using LeGall filters}
  \label{fig:dwtUV}
\end{figure}

\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/uu}
    \caption{\texttt{UU}}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/uv}
    \caption{\texttt{UV}}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/vu}
    \caption{\texttt{VU}}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/vv}
    \caption{\texttt{VV}}
  \end{subfigure}\hspace{1em}%
  \caption{Images obtained by further use of LeGall filters}
  \label{fig:dwtdouble}
\end{figure}


\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/Equal/1}
    \caption{1 Level}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/Equal/3}
    \caption{3 Levels}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/Equal/5}
    \caption{5 Levels}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/Equal/7}
    \caption{7 Level}
  \end{subfigure}\hspace{1em}%
  \caption{Images compressed using a binary wavelet tree scheme with a constant quantization step}
  \label{fig:constantbwtcompression}
\end{figure}



\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/Varying/1}
    \caption{1 Level}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/Varying/3}
    \caption{3 Levels}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/Varying/5}
    \caption{5 Levels}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/Varying/7}
    \caption{7 Level}
  \end{subfigure}\hspace{1em}%
  \caption{Images compressed using a binary wavelet tree scheme with a varying quantization step}
  \label{fig:varyingbwtcompression}
\end{figure}

\begin{figure}[H]
\centering
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/bridge/X}
    \caption{Uncompressed}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/bridge/1}
    \caption{1 Level}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/bridge/4}
    \caption{4 Levels}
  \end{subfigure}\hspace{1em}%
  \begin{subfigure}[t]{0.18\textwidth}
    \centering
    \includegraphics[height=\textwidth]{../ImagesII/dwt/quantdwt/bridge/7}
    \caption{7 Level}
  \end{subfigure}\hspace{1em}%
  \caption{The bridge image compressed using a binary wavelet tree scheme with a varying quantization step}
  \label{fig:varyingbwtbridge}
\end{figure}


\end{document}
