function [ RL ] = runlength( X )
%RUNLENGTH Summary of this function goes here
%   Detailed explanation goes here
numzero=0;
RL=zeros(numel(X),2);
rlrow=1;
for i=1:numel(X)
    if ((X(i)==0) & (i~=numel(X)))
        numzero=numzero+1;
    else
        RL(rlrow,:)=[numzero X(i)];
        rlrow=rlrow+1;
        numzero=0;
    end
end
RL=RL(1:(rlrow-1),:);

end
