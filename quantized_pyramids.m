function [P1 P2 P3 P4 ] = quantized_pyramids( X )
%QUANTIZED_PYRAMIDS Summary of this function goes here
%   Detailed explanation goes here

% create the pyramids for X0 through to X4  

h = [1 2 1]/4;

step = 17;

X1 = rowdec( rowdec( X, h )', h )';
Y0 = X - rowint( rowint( X1, 2 .* h )', 2.*h )';

X2 = rowdec( rowdec( X1, h )', h )';
Y1 = X1 - rowint( rowint( X2, 2 .* h )', 2.*h )';

X3 = rowdec( rowdec( X2, h )', h )';
Y2 = X2 - rowint( rowint( X3, 2 .* h )', 2.*h )';

X4 = rowdec( rowdec( X3, h )', h )';
Y3 = X3 - rowint( rowint( X4, 2 .* h )', 2.*h )';

% Quantize

X1 = quantise( X1, step );
X2 = quantise( X2, step );
X3 = quantise( X3, step );
X4 = quantise( X4, step );

Y0 = quantise( Y0, step );
Y1 = quantise( Y1, step );
Y2 = quantise( Y2, step );
Y3 = quantise( Y3, step );

% create images from the Pyramids

% 4th Pyramid

Z3 = Y3 + rowint( rowint( X4, h*2 )', h*2 )';

Z2 = Y2 + rowint( rowint( Z3, h*2 )', h*2 )';

Z1 = Y1 + rowint( rowint( Z2, h*2 )', h*2 )';

P4 = Y0 + rowint( rowint( Z1, h*2 )', h*2 )';

% 3rd Pyramid


Z2 = Y2 + rowint( rowint( X3, h*2 )', h*2 )';

Z1 = Y1 + rowint( rowint( Z2, h*2 )', h*2 )';

P3 = Y0 + rowint( rowint( Z1, h*2 )', h*2 )';

% 2nd Pyramid

Z1 = Y1 + rowint( rowint( X2, h*2 )', h*2 )';

P2 = Y0 + rowint( rowint( Z1, h*2 )', h*2 )';

% 1st Pyramid

P1 = Y0 + rowint( rowint( X1, h*2 )', h*2 )';

% display the Pyramids along with X

draw( beside( X, beside( P1, beside( P2, beside( P3, P4 ) ) ) ) );

% Display Errors

Diff_1 = X-P1;
RMS_Error_1 = sqrt(sum( Diff_1(:).^2 )/numel(Diff_1))

Diff_2 = X-P2;
RMS_Error_2 = sqrt(sum( Diff_2(:).^2 )/numel(Diff_2))

Diff_3 = X-P3;
RMS_Error_3 = sqrt(sum( Diff_3(:).^2 )/numel(Diff_3))

Diff_4 = X-P4;
RMS_Error_4 = sqrt(sum( Diff_4(:).^2 )/numel(Diff_4))
end

