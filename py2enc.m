function [Y0, Y1, X2] = py2enc(X);

h = [1 2 1]/4;

X1 = rowdec( rowdec( X, h )', h )';
Y0 = X - rowint( rowint( X1, 2 .* h )', 2.*h )';

X2 = rowdec( rowdec( X1, h )', h )';
Y1 = X1 - rowint( rowint( X2, 2 .* h )', 2.*h )';

