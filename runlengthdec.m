function [ X ] = runlengthdec( RL )
%RUNLENGTHDEC Summary of this function goes here
%   Detailed explanation goes here

X = zeros(size(RL,1)+sum(RL(:,1)),1);
xpos=1;
for i = 1:size(RL,1)
    thisrunlength=RL(i,1);
X(xpos:(xpos+thisrunlength))=[zeros(thisrunlength,1); RL(i,2)];
xpos=xpos+thisrunlength+1;

    end